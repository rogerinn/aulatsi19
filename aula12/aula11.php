<?php

class Usuario {
    private $id;
    private $nome;
    private $email; 
    private $senha; 
    private $objDB; 
    
    public function __construct(){
        echo "aqui esta a conexao com o banco\n";

        $this->objDB = new mysqli('Localhost','root', '','aulaphp');
        
        
    }

    public function setId (int $id) {
        $this -> id = $id;
    }

    public function setNome (string $nome) {
        $this -> nome = $nome;
    }

    public function setEmail (string $email) {
        $this -> email = $email;
    }

    public function setSenha (string $senha) {
        $this -> senha = $senha;
    }

    public function getId (int $id) : int {
        return $this -> id;
    }

    public function getNome (string $nome) : string {
        return $this -> nome;
    }

    public function getEmail (string $email) : string {
        return $this -> email;
    }

    public function getSenha (string $senha) : string {
        return $this -> senha; 
    }
    
    public function saveUsuario(){ 
       
        $objStmt = $this->objDB->prepare('REPLACE INTO usuario_tb (id, nome, email, senha) VALUES(?,?,?,?)');
        
        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);

        if($objStmt->execute() ){
            return true;
        }else{
            return false;
        }
    }

    // public function saveDelete(){
    //     $objStmt = $this->objDB->prepare('DELETE * from usuario_tb WHERE (?)');
    //     $objStmt->bind_param('i', $this->id);
    //     if($objStmt->execute()){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }

    public function saveDelete(){
        $objStmt = $this->objDB->prepare('SELECT * from usuario_tb WHERE (?)');
        $objStmt->bind_param('i', $this->id);
        if($objStmt->execute()){
            return true;
        }else{
            return false;
        }
    }


    public function __destruct() {
        echo "<br>Fechando conexao com SGDB";
        unset($this->objDB);
    }

}