<!DOCTYPE html>
<html lang="en">
<body>
<style>
        table {
            font-family: arial, sans-serif;
            width: 15%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
        }
    </style>
<?php

echo "<pre>";

//mostra detalhes do ambiente
//phpinfo();

// echo "Seu IP é: " . $_SERVER['REMOTE_ADDR'];

//vetor com mais de uma dimensão

$alunos[0]['nome'] = 'Roger';
$alunos[0]['Facebook'] = 'fb.gg';

$alunos[1]['nome'] = 'Walter';
$alunos[1]['Facebook'] = 'fb.gg';

$alunos[2]['nome'] = 'Bia';
$alunos[2]['Facebook'] = 'fb.gg';

$alunos[3]['nome'] = 'Vini';
$alunos[3]['Facebook'] = 'fb.gg';

$alunos[4]['nome'] = 'Dj';
$alunos[4]['Facebook'] = 'fb.gg';

//var_dump($alunos);

echo '<table>';
$cor = 'null';
foreach($alunos as $ind => $linha){
    if($cor == 'gray'){
        $cor = 'white';
    }else{
        $cor = 'gray';
    }
    //$cor = $cor == 'gray' ? 'white' : 'gray';

    echo "<tr bgcolor='$cor'>
<td>
    {$linha['nome']}
</td>
<td>
    {$linha['Facebook']}
</td>
</tr>";
}

echo '</table>';

$nome = 'Rogerio Jr';

$posicao = strpos($nome, 'rio');

if($posicao != false){
    
    echo "encontrado na posição $posicao!!";
}

?>
</body>
</html>